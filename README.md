# Performance Projection

## General Points

#### Naming convention

- I try to use flat names like `RiskLevelFilter` for easier access while developing.
- File name should represent the exported convention.

```js
const TextField = () => (...)
export default TextField // should have (TextField.js) as a file name
```

```js
const useFetch = () => (...)
export { useFetch }  // should have (useFetch.js) as a file name
```

***

## _WHY?_

**Replaced all class components with function components**

Because class components are bad for tree shaking and for uglification as method names don't get uglified which increases bundle size.

**Removed all unnecessary mapping**

As a client application, you should try to reduce complexity as much as you can so you don't hurt user experience.

**Styled-components**

As styled-components requires zero configuration, I chose to use it to speed up development.

**FlowJS**

I removed `prop-types` and replaced it with `FlowJS` for static typing as `prop-types` works only with components
and `FlowJS` is samrter that `prop-types`.

**Prettier**
So I focus on development and `prettier` takes care of formatting.

**`@testing-library/react`**
For testing components along with `jest` as a test runner.

***

## _DON'T DO!_
- Dont't mix `require` with `import` when importing modules.
- Don't use `React.createElement` along with `JSX`.
- Be consistent when declaring variables, keep using `const` as long as you won't reassign that variable.
- use `.find` when you're expecting single value, instead of using `.filter()[0]`

***

## _WHAT COULD BE IMPROVED?_

- `App.js` file should be restructured and `<Route />` components should be extracted.
- `useFetch` should accept `path` for fetching different endpoints.
- For real life app, data could be much more, so we can use virtualized list so we don't hurt performance when rendering giant lists.
- Instead of having `if (props.isloading) {...}` at the top of each component to show `<Loader />`, this could be HOC wrapping the component and remove the redundant code.
- Handle error state, similar to how we will handle loading state.
- Adding `stylelint` for validating CSS inside `styled-components`.


***

## _AVAILABLE COMMANDS_

#### Testing

```sh-session
yarn test
```


#### Formatting

```sh-session
yarn prettier
```

#### FlowJS

```sh
yarn flow
```

