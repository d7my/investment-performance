// @flow
import { useState, useEffect } from "react";

const useFetch = (setData: Function) => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("/api/cones");
        const data = await response.json();
        setData(data);
      } catch (error) {
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, [setData, setIsLoading]);

  return [isLoading, error];
};

export { useFetch };
