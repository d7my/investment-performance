import { useTimeSeries } from "./useTimeSeries";

const cones = [
  {
    riskLevel: 3,
    mu: 0.0216,
    sigma: 0.0215
  },
  {
    riskLevel: 4,
    mu: 0.0251,
    sigma: 0.0247
  }
];

const emptyTimeSeries = {
  median: [],
  lower05: [],
  upper95: []
};

describe("useTimeSeries", () => {
  describe("when not passing risk level", () => {
    it("shouldn't throw", () => {
      expect(() => {
        useTimeSeries(cones);
      }).not.toThrow();
    });
    it("should return an empty time series", () => {
      expect(useTimeSeries(cones)).toEqual(emptyTimeSeries);
    });
  });

  describe("when passing unavailable risk level", () => {
    it("should return an empty time series", () => {
      expect(useTimeSeries(cones, 5, 10000)).toEqual(emptyTimeSeries);
    });
  });

  describe("when passing available risk level", () => {
    it("should return an object with median, lower05 and upper95 as arrays", () => {
      const actual = useTimeSeries(cones, 3, 10000);
      expect(Array.isArray(actual.median)).toEqual(true);
      expect(Array.isArray(actual.lower05)).toEqual(true);
      expect(Array.isArray(actual.upper95)).toEqual(true);
    });
  });
});
