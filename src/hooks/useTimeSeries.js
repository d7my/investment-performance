// @flow
import { calculateTimeSeries } from "../utils/calculateTimeSeries";
import type { ConeType } from "../types/common";
import { DEFAULT_NUMBER_OF_YEARS, DEFAULT_FEE } from "../constants";

const useTimeSeries = (
  cones: Array<ConeType>,
  riskLevel: number,
  initialSum: number
) => {
  const cone = cones.find(cone => cone.riskLevel === riskLevel);

  if (cone) {
    return calculateTimeSeries({
      averageYearlyReturn: cone.mu,
      sigma: cone.sigma,
      years: DEFAULT_NUMBER_OF_YEARS,
      initialSum: initialSum,
      monthlySum: 200,
      fee: DEFAULT_FEE
    });
  }

  return {
    median: [],
    lower05: [],
    upper95: []
  };
};

export { useTimeSeries };
