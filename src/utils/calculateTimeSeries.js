// @flow

type MapDateParams = {
  t: number,
  averageYearlyReturn: number,
  sigma: number,
  fee: number,
  initialSum: number,
  monthlySum: number
};
const mapDate = ({
  t,
  averageYearlyReturn,
  sigma,
  fee,
  initialSum,
  monthlySum
}: MapDateParams) => {
  const yearlyReturn = averageYearlyReturn - fee;
  const monthlyReturn = yearlyReturn / 12;
  const month = t * 12;

  const median =
    initialSum * Math.exp(yearlyReturn * t) +
    (monthlySum *
      Math.exp(monthlyReturn * (month - Math.floor(month))) *
      (Math.exp(monthlyReturn * Math.floor(month)) - 1.0)) /
      (Math.exp(monthlyReturn) - 1.0);

  return {
    median,
    upper95: Math.exp(Math.log(median) + Math.sqrt(t) * sigma * 1.645),
    lower05: Math.exp(Math.log(median) - Math.sqrt(t) * sigma * 1.645)
  };
};

type CalculateTimeSeriesParams = {
  years: number,
  averageYearlyReturn: number,
  sigma: number,
  fee: number,
  initialSum: number,
  monthlySum: number
};
const calculateTimeSeries = ({
  years,
  averageYearlyReturn,
  sigma,
  fee,
  initialSum,
  monthlySum
}: CalculateTimeSeriesParams) => {
  const series = {
    median: [],
    upper95: [],
    lower05: []
  };
  for (let k = 0; k < 12 * years; ++k) {
    const { median, upper95, lower05 } = mapDate({
      t: k / 12,
      averageYearlyReturn,
      sigma,
      fee,
      initialSum,
      monthlySum
    });

    series.median.push(median);
    series.upper95.push(upper95);
    series.lower05.push(lower05);
  }
  return series;
};

export { calculateTimeSeries };
