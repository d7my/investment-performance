const throttle = (func, wait) => {
  let timer = null;

  return (...args) => {
    timer = setTimeout(() => {
      func(...args);
      timer = null;
    }, wait);
  };
};

export { throttle };
