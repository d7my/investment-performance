// @flow
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

const appRootElm = document.getElementById("root");
if (appRootElm) ReactDOM.render(<App />, appRootElm);
