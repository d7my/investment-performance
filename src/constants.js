// @flow
export const DEFAULT_LOCALE = "en-US";
export const DEFAULT_NUMBER_OF_YEARS = 10;
export const YEAR_MONTHS = 12;
export const DEFAULT_FEE = 0.01;
export const INITIAL_SUM = 10000;
export const DEFAULT_RISK_LEVEL = 10;
export const DEFAULT_THROTTLING_TIME_MS = 50;
