import React, { useState } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import RiskLevelFilter from "./components/RiskLevelFilter";
import Table from "./components/Table";
import Chart from "./components/Chart";
import Navbar from "./components/Navbar";
import GlobalStyles from "./GlobalStyles";
import { useFetch } from "./hooks/useFetch";
import { Layout } from "./components/Layout";
import { INITIAL_SUM, DEFAULT_RISK_LEVEL } from "./constants";
import { TextField } from "./components/TextField";

const App = () => {
  const [riskLevel, setRiskLevel] = useState(DEFAULT_RISK_LEVEL);
  const [initialSum, setInitialSum] = useState(INITIAL_SUM);
  const [data, setData] = useState([]);
  const [isLoading, hasError] = useFetch(setData);

  return (
    <Router>
      <GlobalStyles />
      <Navbar />
      <Layout>
        <div>
          {!isLoading && (
            <RiskLevelFilter
              riskLevel={riskLevel}
              onChangeRiskLevel={setRiskLevel}
            />
          )}
          {!isLoading && (
            <TextField
              label="Initial investment"
              value={initialSum}
              type="number"
              minimumValue={INITIAL_SUM}
              onChange={setInitialSum}
            />
          )}
        </div>
        <Route
          exact
          path="/"
          component={() => (
            <Table
              cones={data}
              isLoading={isLoading}
              initialSum={initialSum}
              riskLevel={riskLevel}
            />
          )}
        />
        <Route
          path="/table"
          component={() => (
            <Table
              cones={data}
              isLoading={isLoading}
              initialSum={initialSum}
              riskLevel={riskLevel}
            />
          )}
        />
        <Route
          path="/chart"
          component={() => (
            <Chart
              cones={data}
              isLoading={isLoading}
              initialSum={initialSum}
              riskLevel={riskLevel}
            />
          )}
        />
      </Layout>
    </Router>
  );
};

export default App;
