// flow
import React from "react";
import styled from "styled-components";
import { useTimeSeries } from "../hooks/useTimeSeries";
import Loader from "./Loader";
import type { ConeType } from "../types/common";

const TableContainer = styled.table`
  display: inline-table;
  border: 1px solid #f5f9fa;
  border-spacing: 0;
`;

const TableRow = styled.tr`
  &:nth-of-type(odd) {
    background-color: rgba(236, 244, 245, 0.5);
  }

  &:hover {
    background-color: #eaeeef;
  }
`;

const TableColumn = styled.td`
  padding: 16px 24px;
`;

const TableHead = styled.th`
  padding: 16px;
`;

const TableHeader = () => (
  <thead>
    <tr>
      <TableHead>Month</TableHead>
      <TableHead>Good</TableHead>
      <TableHead>Median</TableHead>
      <TableHead>Bad</TableHead>
    </tr>
  </thead>
);

type TableProps = {
  riskLevel: number,
  initialSum: number,
  isLoading: boolean,
  cones: Array<ConeType>
};

const Table = (props: TableProps) => {
  if (props.isLoading) {
    return <Loader name="table-view" />;
  }

  const timeSeries = useTimeSeries(
    props.cones,
    props.riskLevel,
    props.initialSum
  );
  const months = timeSeries.median.map((v, idx) => idx + 1);
  const dataGood = timeSeries.upper95;
  const dataMedian = timeSeries.median;
  const dataBad = timeSeries.lower05;

  return (
    <TableContainer>
      <TableHeader />
      <tbody>
        {months.map((entry, idx) => (
          <TableRow data-testid={`table-row-${idx}`} key={idx}>
            <TableColumn>{entry}</TableColumn>
            <TableColumn>{dataGood[idx]}</TableColumn>
            <TableColumn>{dataMedian[idx]}</TableColumn>
            <TableColumn>{dataBad[idx]}</TableColumn>
          </TableRow>
        ))}
      </tbody>
    </TableContainer>
  );
};

Table.defaultProps = {
  riskLevel: 3
};

export default Table;
