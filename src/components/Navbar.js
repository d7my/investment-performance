// @flow
import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Header = styled.header`
  padding: 16px;
  background-color: #ecf4f5;
  margin-bottom: 16px;
`;

const NavLink = styled(Link)`
  padding: 16px;
  color: #464141;
  transition: color 0.5s linear, background-color 0.2s linear;

  &:hover {
    color: #333131;
    background-color: #d7e3e4;
  }
`;

const NavList = styled.ul``;

const NavListItem = styled.li`
  display: inline-block;
`;

const Navbar = () => (
  <Header>
    <NavList>
      <NavListItem>
        <NavLink to="/table">Table</NavLink>
      </NavListItem>
      <NavListItem>
        <NavLink to="/chart">Chart</NavLink>
      </NavListItem>
    </NavList>
  </Header>
);

export default Navbar;
