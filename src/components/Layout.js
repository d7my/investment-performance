import * as React from "react";
import styled from "styled-components";

const LayoutContainer = styled.div`
  max-width: 800px;
  margin: auto;
  text-align: center;
`;

type LayoutProps = {
  children: React.Node
};
const Layout = (props: LayoutProps) => (
  <LayoutContainer>{props.children}</LayoutContainer>
);

export { Layout };
