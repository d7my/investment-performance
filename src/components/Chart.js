// @flow
import React, { useRef, useEffect } from "react";
import { Chart as ChartJs } from "chart.js";
import cones from "./../../cones";
import { useTimeSeries } from "../hooks/useTimeSeries";
import Loader from "./Loader";
import type { ConeType } from "../types/common";

const chartOptions = {
  responsive: false,
  scales: {
    xAxes: [
      {
        display: true,
        scaleLabel: {
          display: true,
          labelString: "Years"
        },
        gridLines: {
          drawOnChartArea: false
        }
      }
    ],
    yAxes: [
      {
        display: true,
        scaleLabel: {
          display: true,
          labelString: "Valuation (EUR)"
        }
      }
    ]
  }
};

type ChartProps = {
  riskLevel: number,
  initialSum: number,
  isLoading: boolean,
  cones: Array<ConeType>
};
const Chart = (props: ChartProps) => {
  if (props.isLoading) {
    return <Loader name="chart-view" />;
  }
  const chartElm = useRef(null);

  useEffect(() => {
    const timeSeries = useTimeSeries(cones, props.riskLevel, props.initialSum);
    const labels = timeSeries.median.map((_, idx) =>
      idx % 12 == 0 ? idx / 12 : ""
    );
    const dataMedian = timeSeries.median;
    const dataGood = timeSeries.upper95;
    const dataBad = timeSeries.lower05;

    const data = {
      datasets: [
        {
          data: dataGood,
          label: "Good performance",
          borderColor: "rgba(100, 255, 100, 0.2)",
          fill: false,
          pointRadius: 0
        },
        {
          data: dataMedian,
          label: "Median performance",
          borderColor: "rgba(100, 100, 100, 0.2)",
          fill: false,
          pointRadius: 0
        },
        {
          data: dataBad,
          label: "Bad performance",
          borderColor: "rgba(255, 100, 100, 0.2)",
          fill: false,
          pointRadius: 0
        }
      ],
      labels
    };

    const config = {
      type: "line",
      data,
      options: chartOptions
    };

    const canvas = chartElm.current;
    if (canvas) {
      const context = canvas.getContext("2d");
      const myChart = new ChartJs(context, config);
    }
  }, [chartElm, props.riskLevel]);

  return (
    <canvas
      data-testid="chart-canvas"
      ref={chartElm}
      width={800}
      height={400}
    />
  );
};

export default Chart;
