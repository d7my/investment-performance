import React from "react";
import { render } from "@testing-library/react";
import Chart from "./Chart";

jest.mock("chart.js", () => ({ Chart: class {} }));

const baseProps = {
  riskLevel: 4,
  cones: [
    {
      riskLevel: 3,
      mu: 0.0216,
      sigma: 0.0215
    },
    {
      riskLevel: 4,
      mu: 0.0251,
      sigma: 0.0247
    }
  ]
};

const renderComponent = (props = baseProps) => render(<Chart {...props} />);

describe("<Chart />", () => {
  describe("when data is loading", () => {
    it("should render loader", () => {
      const { getByTestId } = renderComponent({ isLoading: true });
      const loader = getByTestId("chart-view-loader");

      expect(loader).toBeInTheDocument();
    });
  });

  describe("when data is loaded", () => {
    it("shouldn't render loader", () => {
      const { queryByTestId } = renderComponent();
      const loader = queryByTestId("chart-view-loader");

      expect(loader).toEqual(null);
    });

    it("should render chart canvas", () => {
      const { getByTestId } = renderComponent();
      const canvas = getByTestId("chart-canvas");
      expect(canvas).toBeInTheDocument();
    });
  });
});
