import React from "react";
import { render } from "@testing-library/react";
import Table from "./Table";
import { YEAR_MONTHS, DEFAULT_NUMBER_OF_YEARS } from "../constants";

const baseProps = {
  riskLevel: 4,
  cones: [
    {
      riskLevel: 3,
      mu: 0.0216,
      sigma: 0.0215
    },
    {
      riskLevel: 4,
      mu: 0.0251,
      sigma: 0.0247
    }
  ],
  initialSum: 10000
};

const renderComponent = (props = baseProps) => render(<Table {...props} />);

describe("<Table />", () => {
  describe("when data is loading", () => {
    it("should render loader", () => {
      const { getByTestId } = renderComponent({ isLoading: true });
      const loader = getByTestId("table-view-loader");

      expect(loader).toBeInTheDocument();
    });
  });

  describe("when data is loaded", () => {
    it("shouldn't render loader", () => {
      const { queryByTestId } = renderComponent();
      const loader = queryByTestId("table-view-loader");

      expect(loader).toEqual(null);
    });

    it("should render table data rows", () => {
      const { getAllByTestId } = renderComponent();
      const rows = getAllByTestId(/table-row-/);
      expect(rows.length).toEqual(YEAR_MONTHS * DEFAULT_NUMBER_OF_YEARS);
    });
  });
});
