import React from "react";
import { render, fireEvent } from "@testing-library/react";
import RiskLevelSelector from "./RiskLevelFilter";

const baseProps = {
  minRiskLevel: 1,
  maxRiskLevel: 2
};

const renderComponent = (props = baseProps) =>
  render(<RiskLevelSelector {...props} />);

describe("<RiskLevelSelector />", () => {
  describe("when passing minRiskLevel and maxRiskLevel", () => {
    it("should render select element", () => {
      const { getByTestId } = renderComponent();
      const select = getByTestId("risk-level-filter");
      expect(select).toBeInTheDocument();
    });
    it("should render options element", () => {
      const { getAllByTestId } = renderComponent();
      const options = getAllByTestId(/option-/);

      expect(options.length).toEqual(
        baseProps.maxRiskLevel - baseProps.minRiskLevel + 1
      );
    });
  });

  describe("when changing filter value and onChangeRiskLevel is passed", () => {
    it("should call onChangeRiskLevel with selected value", () => {
      const onChangeRiskLevel = jest.fn();
      const selectedValue = "2";
      const { getByTestId } = renderComponent({
        ...baseProps,
        onChangeRiskLevel
      });
      const select = getByTestId("risk-level-filter");

      fireEvent.change(select, { target: { value: selectedValue } });

      expect(onChangeRiskLevel).toHaveBeenNthCalledWith(
        1,
        parseInt(selectedValue)
      );
    });
  });

  describe("when changing filter value and onChangeRiskLevel is not passed", () => {
    it("shouldn't throw an error and should fallback to default method", () => {
      const { getByTestId } = renderComponent({
        ...baseProps,
        onChangeRiskLevel: undefined
      });
      const select = getByTestId("risk-level-filter");

      expect(() => {
        fireEvent.change(select, { target: { value: "2" } });
      }).not.toThrow();
    });
  });
});
