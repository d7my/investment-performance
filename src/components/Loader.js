// @flow

import React from "react";
import styled, { keyframes } from "styled-components";

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Cirlce = styled.div`
  display: inline-block;
  position: relative;
  width: 36px;
  height: 36px;
  border-radius: 50%;

  &::before {
    position: absolute;
    width: inherit;
    height: inherit;
    content: "";
    border: 3px solid rgba(128, 128, 128, 0.1);
    border-radius: inherit;
  }

  &::after {
    position: absolute;
    width: inherit;
    height: inherit;
    content: "";
    border: 3px solid;
    border-radius: inherit;
    border-width: 3px;
    border-style: solid;
    border-color: #a6a6a6 transparent transparent;
    animation: ${rotate} 1s linear infinite;
  }
`;

type LoaderProps = {
  name: string
};

const Loader = (props: LoaderProps) => (
  <Cirlce data-testid={`${props.name}-loader`} />
);

export default Loader;
