// @flow
import React from "react";
import styled from "styled-components";
import { throttle } from "../utils/throttle";
import { DEFAULT_THROTTLING_TIME_MS } from "../constants";

const InputContainer = styled.div`
  display: inline-block;
  padding: 16px;
`;

const InputLabel = styled.label`
  display: block;
  margin-bottom: 4px;
  text-align: left;
  font-size: 14px;
`;

const Input = styled.input`
  border: 1px solid #d8d8d8;
  border-radius: 2px;
`;

type TextFieldProps = {
  name: string,
  label: string,
  value: string,
  type?: string,
  onChange?: (value: string | number) => void
};
const TextField = (props: TextFieldProps) => {
  const onFieldChange = event => {
    if (props.onChange) {
      throttle(props.onChange(event.target.value), DEFAULT_THROTTLING_TIME_MS);
    }
  };

  return (
    <InputContainer>
      <InputLabel htmlFor="">{props.label}</InputLabel>
      <Input
        value={props.value}
        type={props.type || "text"}
        onChange={onFieldChange}
      />
    </InputContainer>
  );
};

export { TextField };
