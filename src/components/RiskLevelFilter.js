// @flow
import React from "react";
import styled from "styled-components";

const RiskLevelFilterContainer = styled.div`
  display: inline-block;
  padding: 16px;
`;

const RiskLevelFilterLabel = styled.label`
  display: block;
  margin-bottom: 4px;
  text-align: left;
  font-size: 14px;
`;

const Select = styled.select`
  width: 100%;
  min-width: 120px;
`;

type RiskLevelFilterProps = {
  onChangeRiskLevel: (riskLevel?: number) => void,
  riskLevel: number,
  minRiskLevel: number,
  maxRiskLevel: number
};

const RiskLevelFilter = (props: RiskLevelFilterProps) => {
  const { minRiskLevel, maxRiskLevel } = props;
  const options = [];
  for (let k = minRiskLevel; k <= maxRiskLevel; ++k) {
    options.push(
      <option data-testid={`option-${k}`} key={k} value={k}>
        {k}
      </option>
    );
  }

  const onChange = event =>
    props.onChangeRiskLevel(parseInt(event.target.value));

  return (
    <RiskLevelFilterContainer>
      <RiskLevelFilterLabel>Risk level:</RiskLevelFilterLabel>
      <Select
        data-testid="risk-level-filter"
        onChange={onChange}
        defaultValue={props.riskLevel}
      >
        {options}
      </Select>
    </RiskLevelFilterContainer>
  );
};

RiskLevelFilter.defaultProps = {
  minRiskLevel: 3,
  maxRiskLevel: 25,
  onChangeRiskLevel: () => {}
};

export default RiskLevelFilter;
