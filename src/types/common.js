// @flow

export type ConeType = {
  riskLevel: number,
  mu: number,
  sigma: number
};
